/*
 * Grid.cpp
 *
 *  Created on: Nov 21, 2013
 *      Author: ataias
 */

#include "Grid.h"
Grid::Grid(int nDimen) {
	srand(time(NULL));
	Grid::nDimension = nDimen;
	dGrid = Grid::allocate(nDimension, true);
	nextGrid = Grid::allocate(nDimension, false);
	counter = 0;
	needsupdate = true;
}

//Destructor
Grid::~Grid() {
	disallocate(Grid::dGrid, Grid::nDimension);
	disallocate(Grid::nextGrid, Grid::nDimension);
}

//Gets average around a point of dGrid and saves it in a different grid
void Grid::getAverage(int Xc, int Yc){
		int x[8]={-1,-1,-1,0,0,1,1,1};
		int y[8]={-1,0,1,-1,1,-1,0,1};
		int nElems=0;
		int px, py;
		double sum=0.0;
		for(int i=0; i<8; i++){
			px=x[i]+Xc;
			py=y[i]+Yc;
			if(px<0 || py <0) continue;
			if(px>nDimension-1 || py>nDimension-1) continue;
			sum+=Grid::dGrid[px][py];
			nElems++;
		}
		/**
		 * Asynchronous update is more or less done here
		 * Instead of updating the current grid, we update other one
		 * and later we just do a fast operation with pointers.
		 * */
		Grid::nextGrid[Xc][Yc]=sum/nElems;
		if(needsupdate) return; //already needs update, so won't bother checking again
		if((Grid::dGrid[Xc][Yc] - Grid::nextGrid[Xc][Yc]) > ERROR) needsupdate = true;
		if((-Grid::dGrid[Xc][Yc] + Grid::nextGrid[Xc][Yc]) > ERROR) needsupdate = true;
}

//Updates grid, must be called only by one thread.
void Grid::update(){
	/**
	 * After all threads have finished calculations we just
	 * need to play with pointers such that the update happens
	 * */
	double** temp = Grid::dGrid;
	Grid::dGrid = Grid::nextGrid;
	Grid::nextGrid = temp;
}

// Allocates a matrix of doubles of dimension nDimension and a pointer for it
double** Grid::allocate(int nDimension, bool random){
	double** ptr;
	ptr = new double*[nDimension];
	for(int i=0; i<nDimension; i++){
		ptr[i]=new double[nDimension];
	}
	for(int i=0; i<nDimension; i++){
		for(int j=0; j<nDimension; j++){
			if(random) ptr[i][j]=(rand() % 360);
			else ptr[i][j]=0.0;
		}
	}
	return ptr;
}

/**
 * Frees memory of a grid given its pointer and its dimension
 * Only for square grids
 * */
void Grid::disallocate(double** ptr, int nDimension){
	for (int i = 0; i < nDimension; ++i){
		delete[] ptr[i];
	}
	delete ptr;
}

//Print dGrid on screen
void Grid::print(){
	for(int i=0; i<Grid::nDimension; i++){
		for(int j=0; j<Grid::nDimension; j++){
			printf("%lf ", dGrid[i][j]);
		}
		cout << "\n";
	}
	cout << "\n";
}

double Grid::getMax(){
	double max = Grid::dGrid[0][0];
	for(int i=0; i<Grid::nDimension; i++){
		for(int j=0; j<Grid::nDimension; j++){
			if(Grid::dGrid[i][j]>max)
				max = Grid::dGrid[i][j];
		}
	}
	return max;
}

double Grid::getMin(){
	double min = Grid::dGrid[0][0];
	for(int i=0; i<Grid::nDimension; i++){
		for(int j=0; j<Grid::nDimension; j++){
			if(Grid::dGrid[i][j]<min)
				min = Grid::dGrid[i][j];
		}
	}
	return min;
}
