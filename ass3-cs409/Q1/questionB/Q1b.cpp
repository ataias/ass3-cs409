/*
 * Q1b.cpp
 *
 *  Created on: Nov 21, 2013
 *  Author: Ataias Pereira Reis
 */

#include <iostream>
#include "Grid.h"
#include <cstdlib>
#include <ctime>
#include <omp.h>
using namespace std;

void run(int timeInSeconds, Grid* grid, int nThreads){
	//Started running!
	time_t start, stop;
	time(&start);
	time(&stop);
	int endTime = ((int) start) + timeInSeconds;
	while(endTime >= ((int) stop)){
		grid->needsupdate = false;
		#pragma omp parallel for
		for(int i=0; i<grid->nDimension; i++){
			for(int j=0; j<grid->nDimension; j++){
				grid->getAverage(i, j);
			}
		}
		grid->update();
		if(!grid->needsupdate) break;
		time(&stop);
	}
}


/**
 * Parse arguments
 * @param argc
 * @param argv
 * @param nDimen pointer to integer whose value will be the dimension of the grid
 * @param nThreads is a pointer to an integer whose value will be the number of threads
 * */
void parseArguments(int argc, char* argv[], int* nDimen, int* nThreads, int* print){
	*nDimen = 10; //default value for dimension
	*nThreads = 4; //default value for threads number
	if(argc>1) *nDimen = atoi(argv[1]);
	if(argc>2) *nThreads = atoi(argv[2]);
	if(argc>3) *print = atoi(argv[3]);
}

int main(int argc, char* argv[]){
	cout << "Started running!\n";
	int nDimension, nThreads;
	int print=0; //boolean indicating if grid will be printed on screen or not
	Grid* grid;
	parseArguments(argc, argv, &nDimension, &nThreads, &print);
        omp_set_num_threads(nThreads);
	grid = new Grid(nDimension);
	cout << "Number of threads: " << nThreads << endl;
	if(print){
		cout << "\nInitial grid: \n";
		grid->print();
	}
	run(10, grid, nThreads);

	if(print){
		cout << "Grid after processing \n";
		grid->print();
	}
	cout << "The maximum absolute difference is " << grid->getMax() - grid->getMin() << endl;
	delete grid;
	cout << "Finished running!\n";
	return 0;
}
