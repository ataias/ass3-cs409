/*
 * Grid.h
 *
 *  Created on: Nov 21, 2013
 *      Author: ataias
 */

#ifndef GRID_H_
#define GRID_H_
#define ERROR 1e-8
#include<iostream>
#include<cstdlib>
#include<ctime>
#include<cstdio>
using namespace std;

class Grid {
public:
	//counter: serves as and ID generator
	int counter;
	//needsupdate: used to check if it is time to stop or not or doing updates
	bool needsupdate;
	double** dGrid;
    double** nextGrid;
	int nDimension;
	/**
	 * Constructor for Grid class
	 * @param enter the dimension of the grid to be created
	 * */
	Grid(int nDimen);

	/**
	 * This destructor frees memory when an object of Grid class is deleted
	 * */
	virtual ~Grid();

	/**
	 * getAverage updates the value in grid "nextGrid" as the average
	 * of its neighborhood elements, while leaving "dGrid" unchanged.
	 * If the update of the value had some significant change,
	 * greater than ERROR, then changes variable needschange.
	 * @param Xc is the x coordinate of the grid
	 * @param Ÿc is the y coordinate of the grid
	 * */
	void getAverage(int Xc, int Yc);

	/**
	 * Allocates memory for a grid
	 * @param enter the dimension of the grid
	 * @param enter a boolean saying if the grid should be
	 * initialized to random values. If not, it is initialized to
	 * zeros
	 * */
	double** allocate(int nDimension, bool random);

	/**
	 * Frees the memory of a grid
	 * @param enter a double pointer to the grid, must be a square grid
	 * @param enter the dimension of the grid
	 * */
	void disallocate(double** grid, int nDimension);

	/**
	 * Updates the grids. This may be done only by one thread, and only after
	 * all the others have finished computing averages and are waiting only
	 * for this update to work more.
	 * */
	void update();

	/**
	 * prints dGrid on the screen. It is useful in case of small grids
	 * */
	void print();

	/**
	 * @return maximum value of the grid
	 * */
	double getMax();

	/**
	 * @return minimum value of the grid
	 * */
	double getMin();
};

#endif /* GRID_H_ */
