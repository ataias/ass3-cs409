/*
 * Grid.h
 *
 *  Created on: Nov 21, 2013
 *      Author: ataias
 */

#ifndef GRID_H_
#define GRID_H_
#define ERROR 1e-10
#include<iostream>
#include<cstdlib>
#include<ctime>
#include<cstdio>
using namespace std;

class Grid {
public:
	//counter: serves as and ID generator
	int counter;
	//needsupdate: used to check if it is time to stop or not or doing updates
	bool needsupdate;
	double** dGrid;
    double** nextGrid;
    int* startLines;
    int* endLines;
	int nDimension;
	int nThreads;
	/**
	 * Constructor for Grid class
	 * @param enter the dimension of the grid to be created
	 * @param enter an integer vector in which each element indicates
	 * the start line where each of the threads will start working
	 * (should have dimension nthreads)
	 * @param enter an integer vector in which each elements indicates
	 * the end line where each of the threads will finish working.
	 * if threads have only one line, start and end lines are the same
	 * @param enter the number of threads. This number must be less or equal
	 * the dimension of the grid!
	 * */
	Grid(int nDimen, int* startLin, int* endLin, int nthreads);

	/**
	 * This destructor frees memory when an object of Grid class is deleted
	 * */
	virtual ~Grid();

	/**
	 * getAverage updates the value in grid "nextGrid" as the average
	 * of its neighborhood elements, while leaving "dGrid" unchanged.
	 * If the update of the value had some significant change,
	 * greater than ERROR, then changes variable needschange.
	 * @param Xc is the x coordinate of the grid
	 * @param Ÿc is the y coordinate of the grid
	 * */
	void getAverage(int Xc, int Yc);

	/**
	 * Allocates memory for a grid
	 * @param enter the dimension of the grid
	 * @param enter a boolean saying if the grid should be
	 * initialized to random values. If not, it is initialized to
	 * zeros
	 * */
	double** allocate(int nDimension, bool random);

	/**
	 * Gets and ID, a number between 0 and (nThreads-1)
	 * This function must be called inside a mutual exclusive block
	 * The first call gets 0, the second call 1 and so on.
	 * When the value is (nThreads-1), the next call returns 0
	 * and the cycle repeats again. It is useful to help address each
	 * thread a certain amount of lines of the grid to compute.
	 * */
	int getID();

	/**
	 * Frees the memory of a grid
	 * @param enter a double pointer to the grid, must be a square grid
	 * @param enter the dimension of the grid
	 * */
	void disallocate(double** grid, int nDimension);

	/**
	 * Updates the grids. This may be done only by one thread, and only after
	 * all the others have finished computing averages and are waiting only
	 * for this update to work more.
	 * */
	void update();

	/**
	 * prints dGrid on the screen. It is useful in case of small grids
	 * */
	void print();

	/**
	 * @return maximum value of the grid
	 * */
	double getMax();

	/**
	 * @return minimum value of the grid
	 * */
	double getMin();
};

#endif /* GRID_H_ */
