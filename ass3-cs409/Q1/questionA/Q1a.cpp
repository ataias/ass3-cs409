/*
 * Q1a.cpp
 *
 *  Created on: Nov 21, 2013
 *  Author: Ataias Pereira Reis
 */

#include <iostream>
#include "Grid.h"
#include <cstdlib>
#include <ctime>
#include <pthread.h>
using namespace std;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

/**
 * */
void divideWork(int** startLines, int** endLines, int nDimension, int nThreads){
	int ideal = nDimension / nThreads;
	int restOfDivision = nDimension % nThreads;
	int* startLin=new int[nThreads];
	int* endLin=new int[nThreads];
	for(int i=0; i<nThreads; i++){
			int work = ideal;
			if(restOfDivision>0){
				work+=1;
				restOfDivision--;
			}
			startLin[i]=0;
			endLin[i]=0;
			if(i>0){
				startLin[i]=endLin[i-1]+1;
				endLin[i]=endLin[i-1]+work;
			} else {
				endLin[i]=work-1;
			}
		}
	*startLines = startLin;
	*endLines = endLin;
	for(int i=0; i<nThreads; i++){
		cout << "Thread " << i << " has lines from " << startLin[i] << " to " << endLin[i] << ".\n";
	}
}

/**
 * */
void* computeGrid(void *gGrid){
	Grid* grid = (Grid*) gGrid;
	int ID;
	pthread_mutex_lock(&mutex);
	ID = grid->getID();
	pthread_mutex_unlock(&mutex);
	//cout << "Thread ID: " << ID << endl;
	if(grid==NULL) {
		cout << "Grid is null! Segmentation fault!\n";
		return NULL;
	}
	//cout << "Will start computing now! " << ID << endl;

	for(int i=grid->startLines[ID]; i<=grid->endLines[ID]; i++){
		for(int j=0; j<grid->nDimension; j++){
		//	cout << "almost there\n";
			grid->getAverage(i, j);
		}
	}
	return NULL;
}

void run(int timeInSeconds, Grid* grid, int nThreads){
	time_t start, stop;
	time(&start);
	time(&stop);
	pthread_t* t;
	t = new pthread_t[nThreads];
	int endTime = ((int) start) + timeInSeconds;
	while(endTime >= ((int) stop)){
		grid->needsupdate = false;
		for(int i=0; i<nThreads; i++)
		pthread_create(&(t[0]), NULL, computeGrid, (void*) grid);
		for(int i=0; i<nThreads; i++) pthread_join(t[i], NULL);
		grid->update();
		if(!grid->needsupdate) break;
		time(&stop);
	}
	delete[] t;
}


/**
 * Parse arguments
 * @param argc
 * @param argv
 * @param nDimen pointer to integer whose value will be the dimension of the grid
 * @param nThreads is a pointer to an integer whose value will be the number of threads
 * */
void parseArguments(int argc, char* argv[], int* nDimen, int* nThreads, int* print){
	*nDimen = 100; //default value for dimension
	*nThreads = 4; //default value for threads number
	if(argc>1) *nDimen = atoi(argv[1]);
	if(argc>2) *nThreads = atoi(argv[2]);
	if(argc>3) *print = atoi(argv[3]);
}

int main(int argc, char* argv[]){
	cout << "Started running!\n";
	int print=0;
	int nDimension, nThreads;
	Grid* grid;
	parseArguments(argc, argv, &nDimension, &nThreads, &print);
	int* startLines = new int[nThreads];
	int* endLines = new int [nThreads];
	divideWork(&startLines, &endLines, nDimension, nThreads);
	grid = new Grid(nDimension, startLines, endLines, nThreads);
	if(print){
		cout << "\nInitial grid: \n";
		grid->print();
	}
	run(10, grid, nThreads);

	if(print){
		cout << "Grid after processing \n";
		grid->print();
	}
	cout << "The maximum absolute difference is " << grid->getMax() - grid->getMin() << endl;
	delete grid;
	cout << "Finished running!\n";
	return 0;
}
